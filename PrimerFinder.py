'''
Created on 25.11.2010.

@author: Tin Franovic
'''
import LinearHasher as lh
from Bio import SeqIO
def findPrimers(primerLen,files,doRegions):
    '''
    Method which for a given length of primers finds unique primers of the specified
    length in the given files. The doRegions argument denotes if after finding the
    unique primers, the user wants as output also the unique regions. 
    '''
    seqs=[]
    dicts=[]
    i=1
    for file in files:
        f=open(file)
        for seq in SeqIO.parse(f, "fasta"):
            seqs.append(seq)
            dicts.append({})
        i+=1
    if doRegions:
        uniqRegions=[]
    for i in range(len(seqs)):
        hasher=lh.LinearHasher(seqs[i].seq[:primerLen])
        if doRegions:
            uniqRegions.append([])
            for j in range(primerLen+1):
                uniqRegions[i].append(False)
        for j in range(len(seqs[i].seq)-primerLen+1):
            if doRegions:
                uniqRegions[i].append(False)
            hasher.reHash(seqs[i][j+primerLen-1],j)
            key=hasher.getHash()
            unique=True
            for l in range(i):
                if key in dicts[l]:
                    unique=False
                    dicts[l][key]=None
            if unique and not (key in dicts[i]):
                dicts[i][key]=j
    primers=[]
    for i in range(len(seqs)):
        primers.append([])
        for v in dicts[i].values():
            if v!=None:
                primers[i].append(seqs[i].seq[v:v+primerLen])
                if doRegions:
                    for j in range(v,v+primerLen):
                        uniqRegions[i][j]=True
    if doRegions:
        return (seqs, primers, uniqRegions)
    return (seqs, primers)

def processUniqRegions(uniqRegions):
    '''
    Method which processes unique regions in order to prepare them for output.
    '''
    ret=[]
    k=0
    for seq in uniqRegions:
        ret.append([])
        s=0
        for i in range(len(seq)):
            if seq[i]:
                s+=1
            else:
                if s!=0:
                    ret[k].append((i-s,i))
                s=0
        if s!=0:
            ret[i].append(len(seq)-s,len(seq))
        k+=1
    return ret

def drawGraphs(uR, seqs):
    '''
    Method which draws graphs of unique regions (in red). The Matplotlib package
    is required.
    '''
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib.ticker as tick
    y = np.arange(0, 1, 0.1)
    fig = plt.figure(figsize=(10,5))
    fig.subplots_adjust(hspace=3)
    for i in range(len(uR)):
        ax = fig.add_subplot(len(uR), 1, i+1)
        ax.autoscale(True,'both',True)
        ax.set_xlabel(seqs[i].id)
        ax.yaxis.set_major_locator(tick.NullLocator())
        ax.fill_betweenx(y, 0, len(seqs[i]),facecolor="blue")
        for j in range(len(uR[i])):
            ax.fill_betweenx(y, uR[i][j][0], uR[i][j][-1], facecolor="red")
    plt.show()    

if __name__ == '__main__':
    from time import clock
    import getopt, sys, os, re
    if sys.argv<2:
        print >> sys.stderr, "Wrong input arguments!"
        print >> sys.stderr, "Usage: python PrimerFinder.py [-o: output directory] [--regions] [--graphic] input files"
        sys.exit(-1)
    (opts, files) = getopt.getopt(sys.argv[1:], 'o:', ['regions', 'graphic'])
    outDir='results'
    doGraphic=False
    doRegions=False
    for (opt,arg) in opts:
        if opt in ('-o', '-O'):
            outDir=arg
        if opt=='--regions':
            doRegions=True
        if opt=='--graphic':
            doGraphic=True
    if doGraphic:
        doRegions=True
    c=clock()
    if doRegions:
        (seqs,primers,uniqRegions)=findPrimers(20, files, doRegions)
    else:
        (seqs,primers)=findPrimers(20, files, doRegions)
    for i in range(len(seqs)):
        fName=re.sub('\..*', '', os.path.basename(files[i]))
        file = open(outDir + os.path.sep + fName + ".prim", 'w')
        asc=str(seqs[i].id)
        file.write(asc + "\n")
        print "Total primers found for " + asc + ": " + str(len(primers[i]))
        for primer in primers[i]:
            file.write(str(primer) + "\n")
        file.close()
    print "Total runtime: " + str(clock()-c) + " seconds."
    if doRegions:
        print "Writing unique regions."
        uR=processUniqRegions(uniqRegions)
        for i in range(len(seqs)):
            fName=re.sub('\..*', '', os.path.basename(files[i]))
            file = open(outDir + os.path.sep + fName + ".ur", 'w')
            asc=str(seqs[i].id)
            file.write(asc + "\n")
            for region in uR[i]:
                file.write(str(region)+"\n")
            print "Total unique regions found for " + asc + ": " + str(len(uR[i])) + "."
            file.close()
    if doGraphic:
        drawGraphs(uR, seqs)