'''
Created on 24.11.2010.

@author: Tin Franovic
'''

class LinearHasher:
    '''
    A class which uses the rolling hash algorithm to compute the hash of a string.
    '''
    __B=4
    def __init__(self, string):
        '''
        Constructor which accepts the initial string as argument.
        '''
        self.__string=string
        self.__hash=0
        self.__newHash()
        self.__lpos=0
        
    def __charConvert(self, char):
        '''
        Method which converts a character to its numeric representation in base 4.
        That is: A->0, T->1, C->2, G->3.
        '''
        if char=="A":
            return 0
        elif char=="T":
            return 1
        elif char=="C":
            return 2
        elif char=="G":
            return 3
        else :
            raise IOError('Input file contains invalid character: ' + char)
            
    def __newHash(self):
        '''
        Private method which initializes the hash using the first string.
        '''
        exp=len(self.__string)-1
        for c in self.__string:
            self.__hash+=self.__charConvert(c)*pow(self.__B, exp)
            exp-=1
            
    def reHash(self, char, lpos):
        '''
        Method which uses the rolling hash algorithm to produce the hash for the
        string which results by moving by one character forward in the sequence.
        '''
        if self.__lpos==lpos:
            return
        exp=len(self.__string)-1
        self.__hash-=self.__charConvert(self.__string[0])*pow(self.__B,exp)
        self.__hash*=self.__B;
        self.__hash+=self.__charConvert(char)
        self.__string=self.__string[1:]+char
        self.__lpos=lpos
        
    def getHash(self):
        '''
        Method which returns the current hash value.
        '''
        return self.__hash
        